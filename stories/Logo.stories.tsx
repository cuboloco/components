import * as React from 'react'

import { storiesOf } from '@storybook/react'
import { Logo } from '../src'

storiesOf('Logo', module).add('with animation', () => <Logo />)

storiesOf('Logo', module).add('with color', () => <Logo color={'pink'} ballColor={'darkgrey'} />)

storiesOf('Logo', module).add('with alternative size', () => (
  <>
    <Logo size={50} />
    <Logo size={150} />
    <Logo size={280} />
  </>
))

storiesOf('Logo', module).add('without animation', () => <Logo animate={false} />)

storiesOf('Logo', module).add('with href', () => <Logo href="https://example.com" />)

storiesOf('Logo', module).add('with onClick', () => <Logo onClick={() => alert('Hi there')} />)
