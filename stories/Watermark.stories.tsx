import * as React from 'react'

import { storiesOf } from '@storybook/react'
import { Watermark, Logo } from '../src'

storiesOf('Watermark', module).add('with no props', () => <Watermark />)

storiesOf('Watermark', module).add('with overwritten href', () => <Watermark href="https://example.com" />)
