// Load Nunito Sans typeface
import 'typeface-nunito-sans'
import 'typeface-montserrat'

// re-export components (this will allow us to import them directly from '@cuboloco/components')
export { Watermark } from './components/Watermark'
export { Logo } from './components/Logo'
