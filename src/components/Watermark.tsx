import * as React from 'react'

import styled from 'styled-components'
import { Logo } from './Logo'

type Props = {
  textColor?: string
} & React.ComponentProps<typeof Logo>

const Container = styled.div`
  display: inline-flex;
  justify-content: center;
  align-items: center;
`

const LocoText = styled.h6<{ textColor: string }>`
  flex: 1 0;
  font-family: 'Montserrat', sans-serif;
  margin: 0;
  margin-left: 10px;
  font-size: 18px;
  font-weight: 900;
  color: ${(p) => p.textColor};
`

export const Watermark = (props: Props) => {
  const baseTextColor = props.textColor || 'rgba(10, 10, 40, 0.9)'

  return (
    <Container>
      <Logo size={42} href="https://cuboloco.io" {...props} />
      <LocoText textColor={baseTextColor}>cuboloco.io</LocoText>
    </Container>
  )
}
