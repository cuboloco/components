import * as React from 'react'
import { useSpring, animated, config } from 'react-spring'
import styled, { css } from 'styled-components'
import { darken, desaturate, lighten } from 'polished'

const Chevron = styled.div<{ baseColor: string; secondaryColor: string }>`
  display: inline-block;
  position: relative;
  height: 71%;
  width: 90%;

  &:before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 50%;
    background: ${(p) => p.baseColor};
    transform: skew(0deg, 25deg);
  }

  &:after {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    height: 100%;
    width: 50%;
    background: ${(p) => p.secondaryColor};
    transform: skew(0deg, -25deg);
  }
`

const ChevronContainerA = styled.div`
  box-sizing: border-box;
  position: absolute;
  width: 100%;
  height: 100%;
  text-align: center;
  padding: 12% 0;
  top: 0%;
  margin-top: 21%;
`

const ChevronContainerB = styled(ChevronContainerA)`
  margin: 0;
  ${Chevron} {
    transform: scaleY(-1) scaleX(-1);
  }
`

const Ball = styled(animated.div)<{ color: string }>`
  position: absolute;
  top: 15%;
  left: 25%;
  width: 50%;
  height: 50%;
  border-radius: 50%;
  background: ${(p) => p.color};
  transition: transform 100ms ease-in-out;
`

const containerStyling = (size?: number) => css`
  position: relative;
  display: inline-block;
  width: ${size}px;
  height: ${size}px;
`

const Container = styled.div<{ size?: number }>`
  ${(p) => containerStyling(p.size)}
`

const AnchorContainer = styled.a<{ size?: number }>`
  &:hover {
    cursor: pointer;

    ${Ball} {
      transform: translateY(-15%);
    }
  }

  &:active {
    ${Ball} {
      transition-duration: 50ms;
      transform: translateY(10%);
    }
  }

  ${(p) => containerStyling(p.size)}
`

type LogoProps = {
  color?: string
  ballColor?: string
  size?: number
  animate?: boolean

  // setting one of these, will result in a hover-effect
  href?: string
  onClick?: () => void
}

export const Logo = (props: LogoProps) => {
  const baseColor = props.color || '#54B7FF'
  const darkColor = darken(0.04, baseColor)
  const backgroundColor = lighten(0.5, desaturate(0.7, baseColor))
  const darkBackgroundColor = darken(0.04, backgroundColor)
  const ballColor = props.ballColor || '#ffcc80'

  const showHower = props.href !== undefined || props.onClick !== undefined
  const ContainerComponent = showHower ? AnchorContainer : Container
  const containerProps = { onClick: props.onClick, href: props.href }

  const ballStyling = useSpring({
    from: { opacity: 0, top: '-120%' },
    to: { opacity: 1, top: '15%' },
    config: config.stiff,
    delay: 1000,
  })

  const size = props.size || 120

  return (
    <ContainerComponent size={size} {...containerProps}>
      <ChevronContainerB>
        <Chevron baseColor={backgroundColor} secondaryColor={darkBackgroundColor} />
      </ChevronContainerB>
      <Ball style={props.animate === false ? undefined : ballStyling} color={ballColor} />
      <ChevronContainerA>
        <Chevron baseColor={baseColor} secondaryColor={darkColor} />
      </ChevronContainerA>
    </ContainerComponent>
  )
}
